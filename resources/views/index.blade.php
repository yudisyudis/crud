@extends('master')

@section('content')
<div class="content-wrapper">
    <div class="mt-3 ml-5">
        <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>


            </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $data)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$data->nama}}</td>
                <td>{{$data->umur}}</td>
                <td>{{$data->bio}}</td>

                <td> 
                    <a href="cast/{{$data->id}}" class="btn btn-info btn-sm">info</a>
                    <a href="cast/{{$data->id}}/edit" class="btn btn-success btn-sm">edit</a>
                    <form action="cast/{{$data->id}}" method="post" style = display:inline-block;>
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>

            </tr>
            @endforeach
        </tbody>
        <a class="btn btn-link" href="cast/create">Input Data</a>
        </table>
    </div>
</div>
@endsection