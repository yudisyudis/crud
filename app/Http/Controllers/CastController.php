<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('create');
    }

    public function store(Request $request){
        //dd($request->all());
        $validated = $request -> validate([
            'nama' => 'required'
        ]);
            
        $query = DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']

        ]);
        return redirect('/cast');

    }

    public function index(){
        $data = DB::table('cast')->get();
        //dd($data);
        return view('index', compact('data'));
    }

    public function show($id){
        $data1 = DB::table('cast')->where('id',$id)->first();
        //dd($data1);
        return view('show', compact('data1'));
    }

    public function edit($id){
        $data2 = DB::table('cast')->where('id',$id)->first();
        //dd($data);
        return view('edit', compact('data2'));
    }

    public function update($id, Request $request){
        $update = DB::table('cast')
              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                  'umur' => $request['umur'],
                  'bio' => $request['bio']
                ]);
                return redirect('/cast');

    }

    public function hapus($id){
        $hapus = DB::table('cast')
            ->where('id', $id)
            ->delete();
            return redirect('/cast');
    }

};
